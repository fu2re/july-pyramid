"""
django widgets base module

author: Marcin Nowak
license: BSD
"""

from django.forms.widgets import MediaDefiningClass
from django.forms.widgets import Widget as BaseWidget
from django.template.loader import get_template
from loading import registry

def context_wrapper(wrapped_method, context):
    """
    This decorator prepares args and kwargs for wrapped method.
    Backward compatibility is achieved by inspecting method args.

    Background:
      Sometimes access to template Context is needed within widget.
      render() method has context argument, but get_context() not.

      get_context() args are inspected and context is set as keyword
      argument, when `context` argument exist in method`s declaration.
    """
    import inspect
    kwargs = {}

    _ctxargs = inspect.getargspec(wrapped_method)[0] # Python2.5 compatibility
    if 'context' in _ctxargs:
        kwargs['context'] = context

    def wrapper(widget, *args):
        return wrapped_method(widget, *args, **kwargs)

    return wrapper


class WidgetMeta(MediaDefiningClass):
    """
    initializes widget classes
    automatically adds widget instance into registry
    """

    def __init__(mcs, name, bases, attrs):
        MediaDefiningClass.__init__(mcs, name, bases, attrs)
        if 'template' not in attrs:
            mcs.template = None
        mcs.template_instance = None
        if name is not 'Widget':
            registry.register(name, mcs())


class BaseWidget(object):
    is_hidden = False          # Determines whether this corresponds to an <input type="hidden">.
    needs_multipart_form = False # Determines does this widget need multipart form
    is_localized = False
    is_required = False

    class Meta:
        def __new__(cls, name, bases, attrs):
            new_class = super(MediaDefiningClass, cls).__new__(cls, name, bases,
                                                               attrs)
            if 'media' not in attrs:
                new_class.media = media_property(new_class)
            return new_class

    def __init__(self, attrs=None):
        if attrs is not None:
            self.attrs = attrs.copy()
        else:
            self.attrs = {}

    def __deepcopy__(self, memo):
        obj = copy.copy(self)
        obj.attrs = self.attrs.copy()
        memo[id(self)] = obj
        return obj

    def subwidgets(self, name, value, attrs=None, choices=()):
        """
        Yields all "subwidgets" of this widget. Used only by RadioSelect to
        allow template access to individual <input type="radio"> buttons.

        Arguments are the same as for render().
        """
        yield SubWidget(self, name, value, attrs, choices)

    def render(self, name, value, attrs=None):
        """
        Returns this Widget rendered as HTML, as a Unicode string.

        The 'value' given is not guaranteed to be valid input, so subclass
        implementations should program defensively.
        """
        raise NotImplementedError

    def build_attrs(self, extra_attrs=None, **kwargs):
        "Helper function for building an attribute dictionary."
        attrs = dict(self.attrs, **kwargs)
        if extra_attrs:
            attrs.update(extra_attrs)
        return attrs

    def value_from_datadict(self, data, files, name):
        """
        Given a dictionary of data and this widget's name, returns the value
        of this widget. Returns None if it's not provided.
        """
        return data.get(name, None)

    def _has_changed(self, initial, data):
        """
        Return True if data differs from initial.
        """
        # For purposes of seeing whether something has changed, None is
        # the same as an empty string, if the data or inital value we get
        # is None, replace it w/ ''.
        if data is None:
            data_value = ''
        else:
            data_value = data
        if initial is None:
            initial_value = ''
        else:
            initial_value = initial
        if force_text(initial_value) != force_text(data_value):
            return True
        return False

    def id_for_label(self, id_):
        """
        Returns the HTML ID attribute of this Widget for use by a <label>,
        given the ID of the field. Returns None if no ID is available.

        This hook is necessary because some widgets have multiple HTML
        elements and, thus, multiple IDs. In that case, this method should
        return an ID value that corresponds to the first ID in the widget's
        tags.
        """
        return id_


class Widget(BaseWidget):
    """
    base widget class
    """
    __metaclass__ = WidgetMeta
    template = None

    def __init__(self, *args, **kwargs):
        super(BaseWidget, self).__init__(*args, **kwargs)
        self.template_instance = None

    def get_context(self, value, options):
        """
        returns context dictionary
        output my be customized by options dict
        """
        return {}

    def render(self, context, value=None, attrs=None):
        """
        main render method
        uses "template" class property for rendering
        or needs to be overriden for custom non-template widgets
        """
        if not self.template_instance:
            if not self.template:
                raise RuntimeError('Abstract method Widget.render()\
                        is not implemented')
            self.template_instance = get_template(self.template)

        context.push()
        context.update(context_wrapper(self.get_context, context)(
            value, attrs or {}))
        result = self.template_instance.render(context)
        context.pop()
        return result

