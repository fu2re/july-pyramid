from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

Base = automap_base()

# TODO
engine = create_engine("mysql+pymysql://test:test@localhost/history")

# reflect the tables
Base.prepare(engine, reflect=True)