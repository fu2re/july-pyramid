from datetime import datetime, timedelta
from pytz import timezone
import pytz

utc = pytz.utc
current = timezone('Europe/Moscow')


def now():
    return datetime.utcnow()


def to_utc(dt):
    if not dt.tzinfo:
        dt = dt.replace(tzinfo=current)
    return dt.astimezone(utc).replace(tzinfo=None)


def to_local(dt):
    """
    should be used NEVER.
    :param dt:
    :return:
    """
    if not dt.tzinfo:
        dt = dt.replace(tzinfo=utc)
    return dt.astimezone(current)

# class __datetime__(datetime):
#     def __new__(self, year, month, day, hour=None, minute=None, second=None, microsecond=None, tzinfo=None):
#         dt = __datetime__(year, month, day, hour, minute, second, microsecond, tzinfo)
#         if not tzinfo:
#             return to_utc(utc.localize(dt)).replace(tzinfo=None)
#
#         return to_utc(dt)



