import argparse
import os
import re
import sys
import textwrap
from paste.deploy.loadwsgi import appconfig
from july.utils import lazy


class Config(object):
    @lazy
    def __settings__(self):
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
        )
        parser.add_argument(
            'config_uri',
            nargs='?',
            default=None
        )
        args, argv = parser.parse_known_args(sys.argv[1:])
        args = vars(args)
        return appconfig(
            'config:%(config_uri)s' % args,
            'mainapp',
            relative_to='.'
        )

    def __getitem__(self, item):
        return self.__settings__[item]

    def get(self, *args, **kwargs):
        return self.__settings__.get(*args, **kwargs)

    @lazy
    def project_dir(self):
        return os.path.join(os.getcwd(), self.__settings__['app_name'])

    def get_dir(self, item):
        if item in self.__settings__:
            print (self.project_dir, self.__settings__[item])
            return os.path.join(self.project_dir, self.__settings__[item])

settings = Config()
