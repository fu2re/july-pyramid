import bcrypt
from pyramid.security import remember, forget


class EmailBackend:
    def __init__(self, user_model):
        self.object_class = user_model

    @staticmethod
    def hash_password(pw):
        pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
        return pwhash.decode('utf8')

    @staticmethod
    def check_password(pw, hashed_pw):
        expected_hash = hashed_pw.encode('utf8')
        return bcrypt.checkpw(pw.encode('utf8'), expected_hash)

    def get_user(self, email, **kwargs):
        return self.object_class.get(email=email)

    def check_user(self, email, password, **kwargs):
        user = self.get_user(email)
        if user:
            return user.check_password(password)

    @staticmethod
    def authenticate(request, user):
        request.session['user_data'] = user.to_dict()
        return remember(request, user.id)

    @staticmethod
    def forget(request):
        del request.session['user_data']
        return forget(request)