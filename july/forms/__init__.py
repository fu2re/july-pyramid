import inspect
import six
from wtforms import *
from wtforms import Form as WTForm
from wtforms.compat import iteritems
from wtforms.form import FormMeta
from wtforms.compat import with_metaclass
from july.utils import lazy
from july.db import get_fields
from .fields import Converter


class Form(WTForm):
    def __process_field__(self, field):
        processor = getattr(self, 'process_%s' % field.name, None)
        return processor(field) if processor and callable(processor) else field.data

    @lazy
    def data(self):
        return dict((name, self.__process_field__(f)) for name, f in iteritems(self._fields))

    @lazy
    def extra(self):
        __keys__ = [
            K.lstrip('process_') for K in dir(self)
            if K.startswith('process_') and K not in self.data
        ]
        return {
            K: self.__process_field__(type(K, (object,), {'data': None, 'name': K}))
            for K in __keys__
        }


class ModelFormMeta(FormMeta):
    def __new__(cls, name, bases, attrs):
        meta = attrs.get('Meta')
        new_class = super(ModelFormMeta, cls).__new__(cls, name, bases, attrs)

        if meta and meta.object_class:
            for name, attr in get_fields(meta.object_class):
                field, validators = Converter.convert(attr)
                if field:
                    setattr(new_class, name, field(name, validators))

        return new_class

class ModelForm(six.with_metaclass(ModelFormMeta, Form)):
    pass
    # def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, **kwargs):
    #     meta_obj = self._wtforms_meta(
    #         meta
    #     )
    #     if meta is not None and isinstance(meta, dict):
    #         meta_obj.update_values(meta)
    #     super(Form, self).__init__(self._unbound_fields, meta=meta_obj, prefix=prefix)
    #
    #     for name, field in iteritems(self._fields):
    #         # Set all the fields to attributes so that they obscure the class
    #         # attributes with the same names.
    #         setattr(self, name, field)
    #     self.process(formdata, obj, data=data, **kwargs)






    # def process(self, *args, **kwargs):
    #     super(Form, self).process(*args, **kwargs)
    #     self.data.update()
    #     self.data = {K: self.__process_field__(K, V) for K, V in self.data.items()}
