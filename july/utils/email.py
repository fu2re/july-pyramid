#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from pyramid_mailer.mailer import Mailer as PyramidMailer
from pyramid_mailer.message import Attachment, Message
from pyramid.renderers import render
from july.conf import settings
from july.utils import lazy


class Mailer():
    @lazy
    def __mailer__(self):
        return PyramidMailer.from_settings({
            'mail.host': settings['mail.host'],
            'mail.port': settings['mail.port']
        })

    def send_email(self,
                   subject,
                   recipients,
                   body=None,
                   txt=None,
                   context=None,
                   request=None,
                   template=None,
                   txt_template=None,
                   sender=None,
                   attachments=None):
        """

        :param subject:
        :param recipients:
        :param body:
        :param txt:
        :param context:
        :param request:
        :param template:
        :param txt_template:
        :param sender:
        :param attachments: list of ("photo.jpg", "image/jpg", photo_data)
        :return:
        """
        assert txt or ((txt_template or template) and context is not None)
        if not recipients or len(recipients) == 0:
            return

        subject = subject.strip()
        attachments = attachments or []

        context['host'] = settings.get('storage.host', '')

        if not txt and not txt_template:
            txt_template = template.replace('.', '.txt.', template.count('.'))

        if not body and not template.endswith('.txt'):
            body = render(template, context, request=request)

        if not sender:
            sender = settings["mail.default_sender"]

        txt = txt or render(txt_template, context, request=request)
        message = Message(subject=subject,
                          sender=sender,
                          recipients=recipients,
                          body=txt,
                          html=body
                          )
        for attachment in attachments:
            message.attach(Attachment(*attachment))
        self.__mailer__.send_immediately(message)

mailer = Mailer()
