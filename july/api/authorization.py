from uuid import UUID


class ReadOnlyAuthorization(object):
    def can_list_get(self, request, *args, **kwargs):
        return True
    
    def can_list_post(self, request, *args, **kwargs):
        return True
    
    def can_list_put(self, request, *args, **kwargs):
        return True
    
    def can_list_delete(self, request, *args, **kwargs):
        return False
    
    def can_detail_get(self, request, *args, **kwargs):
        return True
    
    def can_detail_post(self, request, *args, **kwargs):
        return False
    
    def can_detail_put(self, request, *args, **kwargs):
        return True
    
    def can_detail_delete(self, request, *args, **kwargs):
        return True


class ModelSessionAuthorization(object):
    # TODO

    def can_list_get(self, request, object_class, *args, **kwargs):
        return request.user.is_superuser or (
            request.user.is_staff and
                request.user.has_perm('%s_read' % object_class) or \
                request.user.has_perm('%s_edit' % object_class)
        )
    
    def can_list_post(self, request, object_class,  *args, **kwargs):
        return request.user.is_superuser or (
            request.user.is_staff and
            request.user.has_perm('%s_edit' % object_class)
        )
    
    def can_list_put(self, request, object_class, *args, **kwargs):
        return request.user.is_superuser or (
            request.user.is_staff and
            request.user.has_perm('%s_edit' % object_class))
    
    def can_list_delete(self, request, object_class, *args, **kwargs):
        return request.user.is_superuser or (
            request.user.is_staff and
            request.user.has_perm('%s_delete' % object_class)
        )
    
    def can_detail_get(self, request, object_class, *args, **kwargs):
        return self.can_list_get(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_post(self, request, object_class, *args, **kwargs):
        return self.can_list_post(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_put(self, request, object_class, *args, **kwargs):
        return self.can_list_put(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_delete(self, request, object_class, *args, **kwargs):
        return self.can_list_delete(
            request, object_class, *args, **kwargs
        )


class ModelTokenAuthorization(object):
    def __init__(self, token_class):
        self.token_class = token_class

    def get_token_uuid(self, request):
        token_str = request.headers.get('X-Authorization')
        if not token_str:
            return None
        return UUID('{%s}' % token_str)

    def can_list_get(self, request, object_class, *args, **kwargs):
        read_perm = '%s_read' % object_class
        edit_perm = '%s_edit' % object_class
        token_uuid = self.get_token_uuid(request)
        token = self.token_class.get(
            lambda x: x.token == token_uuid and (
            x.user.is_superuser or \
            x.user.is_staff and (
                read_perm in x.user.permissions_granted.permission.name or \
                edit_perm in x.user.permissions_granted.permission.name or \
                read_perm in x.user.groups.permissions_granted.permission.name or \
                edit_perm in x.user.groups.permissions_granted.permission.name
            ))
        )

        if token and token.is_valid():
            return token
    
    def can_list_post(self, request, object_class, *args, **kwargs):
        edit_perm = '%s_edit' % object_class
        token_uuid = self.get_token_uuid(request)
        token = self.token_class.get(
            lambda x: x.token == token_uuid and (
            x.user.is_superuser or \
            x.user.is_staff and (
                edit_perm in x.user.permissions_granted.permission.name or \
                edit_perm in x.user.groups.permissions_granted.permission.name
            ))
        )

        if token and token.is_valid():
            return token
    
    def can_list_put(self, request, object_class, *args, **kwargs):
        edit_perm = '%s_edit' % object_class
        token_uuid = self.get_token_uuid(request)
        token = self.token_class.get(
            lambda x: x.token == token_uuid and (
            x.user.is_superuser or \
            x.user.is_staff and (
                edit_perm in x.user.permissions_granted.permission.name or \
                edit_perm in x.user.groups.permissions_granted.permission.name
            ))
        )

        if token and token.is_valid():
            return token
    
    def can_list_delete(self, request, object_class, *args, **kwargs):
        delete_perm = '%s_delete' % object_class
        token_uuid = self.get_token_uuid(request)
        token = self.token_class.get(
            lambda x: x.token == token_uuid and (
            x.user.is_superuser or \
            x.user.is_staff and (
                delete_perm in x.user.permissions_granted.permission.name or \
                delete_perm in x.user.groups.permissions_granted.permission.name
            ))
        )

        if token and token.is_valid():
            return token
    
    def can_detail_get(self, request, object_class, *args, **kwargs):
        return self.can_list_get(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_post(self, request, object_class, *args, **kwargs):
        return self.can_list_post(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_put(self, request, object_class, *args, **kwargs):
        return self.can_list_put(
            request, object_class, *args, **kwargs
        )
    
    def can_detail_delete(self, request, object_class, *args, **kwargs):
        return self.can_list_delete(
            request, object_class, *args, **kwargs
        )
