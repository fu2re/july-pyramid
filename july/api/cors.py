cors_policy = {
    'origins': ('*',),
    'max_age': 42,
    'credentials': True,
    'headers': 'Content-Type,Authorization,X-Authorization,X-Total-Count,X-Content-Range,X-Content-Type,X-Cache'
}