import types

class SimplePagination(object):
    @staticmethod
    def __get_length__(data):
        return len(data)

    @staticmethod
    def __get_crop__(start, end, length, limit=100, max_limit=1000):
        start = int(start or 0)
        end = min(int(end or (start + limit)), length)
        if end - start > max_limit:
            end = start + max_limit
        return start, end

    def __call__(self, data, start=None, end=None, length=None, limit=None, max_limit=None):
        if start is None or end is None:
            length = length or self.__get_length__(data)
            start, end = self.__get_crop__(start, end, length, limit, max_limit)

        return data[start:end]


class PonyPagination(SimplePagination):
    @staticmethod
    def __get_length__(data):
        return data.count()
