# -*- coding: utf-8 -*-
"""Setup file for easy installation"""
from os.path import join, dirname
from setuptools import setup
import sys

name = 'pyramid-july'
version = __import__('july').__version__


def long_description():
    return open(join(dirname(__file__), 'README.md')).read()


def load_requirements():
    return open(join(dirname(__file__), 'requirements.txt')).readlines()


if __name__ == "__main__":
    pv = sys.version_info[:2]
    if pv not in ((3, 3), (3, 4), (3, 5), (3, 6)):
        s = "Sorry, but %s %s requires Python of one of the following versions: 3.3-3.6." \
            " You have version %s"
        print(s % (name, version, sys.version.split(' ', 1)[0]))
        sys.exit(1)
setup(
    name=name,
    version=version,
    author='Andrew Kozlov',
    author_email='darkfu2re@gmail.com',
    description='Django-like framework for Pony and Pyramid integration.',
    license='BSD',
    keywords='pyramid, django, pony',
    url='',
    packages=[
        'july',
        'july.api',
        'july.auth', 'july.auth.backends',
        'july.compat',
        'july.db', 'july.db.converters', 'july.db.converters.file', 'july.db.dbproviders',
        'july.forms',
        'july.utils',
        'july.templates'
    ],
    long_description=long_description(),
    install_requires=load_requirements(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Topic :: Internet',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Developers',
        'Environment :: Web Environment',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3'
    ],
    zip_safe=False
)
